package com.example.vaadindemo;



import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;


@Route
public class RoomsView extends Abstraktview implements Reloader {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private RoomForm form;
    Grid<Room> grid;
    @Override
    public void processRefresh(){
        grid.setItems(roomRepository.findAll());
    }

    @Transactional
    @PostConstruct
    public void init() {
        initView();

        for (int i=0; i<=1; i++){
            Room room = new Room();
            room.setName("Szoba neve");
            System.out.println(room.getName());
            roomRepository.save(room);
        }




        grid = new Grid<>();
        grid.setItems(roomRepository.findAll());
        grid.addColumn(room -> room.getId()).setHeader("Azonosító");
        grid.addColumn(room -> room.getName()).setHeader("Név");
        grid.asSingleSelect().addValueChangeListener(selectionEvent -> {

            if (selectionEvent.getValue() != null) {
                form.setVisible(true);
                form.setRoom(selectionEvent.getValue());
                form.setReloader(this);

                form.setInEdit(true);
            } else {
                form.setVisible(false);
            }
        });


        Button addButton=new Button("New");
        addButton.setIcon(VaadinIcon.PLUS.create());
        addButton.addClickListener(buttonClickEvent -> {
            form.setVisible(true);
            form.setRoom(new Room());
            form.setReloader(this);
            form.setInEdit(false);

        });
        add(new Text("Szobák Nyilvántartása"));
        add(addButton);
        add(grid);
        add(form);

    }
}