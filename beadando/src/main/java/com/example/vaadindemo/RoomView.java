package com.example.vaadindemo;
import com.example.vaadindemo.APPMenuBar;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route
public class RoomView extends Abstraktview{

    public RoomView(){
        initView();
        add(new Text("Terem adatai:"));
        add(new Text("Azonosító(id), "));
        add(new Text("Név(name)"));
    }
}
