package com.example.vaadindemo;


import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;


@SpringComponent
@UIScope
public class RoomForm extends VerticalLayout {

    private Room room;
    private TextField name;
    private Reloader reloader;
    private boolean inEdit;

    private Binder<Room> binder;

    @Autowired
    private RoomRepository repository;

    @PostConstruct
    private void init() {
        binder=new Binder<>(Room.class);
        name=new TextField("Name");

        Button button = new Button();
        button.setText("Törlés");
        button.setIcon(VaadinIcon.TRASH.create());
        button.addClickListener(buttonClickEvent -> {
            setVisible(false);
            repository.delete(room);
            reloader.processRefresh();
        });

        Button editButton = new Button();
        editButton.setText("Szerkeztés");
        editButton.setIcon(VaadinIcon.PENCIL.create());
        editButton.addClickListener(buttonClickEvent -> {
            if(inEdit){
                repository.update((room));
            }else{
                repository.save(room);
            }
            setVisible(false);
            setInEdit(false);
            reloader.processRefresh();
        });



        add(name);
        add(button);
        setVisible(false);
        binder.bindInstanceFields(this);
        add(editButton);



    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        if(room!=null){
            binder.setBean(room);
        }
        this.room = room;
    }
    public boolean isInEdit() {
        return inEdit;
    }

    public void setInEdit(boolean inEdit) {
        this.inEdit = inEdit;
    }

    public Reloader getReloader() {
        return reloader;
    }

    public void setReloader(Reloader reloader) {
        this.reloader = reloader;
    }

}
