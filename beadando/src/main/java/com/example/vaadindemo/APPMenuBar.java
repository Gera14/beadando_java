package com.example.vaadindemo;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class APPMenuBar extends HorizontalLayout {

    public APPMenuBar(){

        buildAChor("Course","/course");
        buildAChor("Courses","/courses");
        buildAChor("Room","/room");
        buildAChor("Rooms","/rooms");
    }
    private void buildAChor(String text,String href){
        Anchor anchor=new Anchor();
        anchor.setText(text);
        anchor.setHref(href);
        add(anchor);
    }
}
