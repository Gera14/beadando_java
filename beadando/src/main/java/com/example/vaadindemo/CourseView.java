package com.example.vaadindemo;


import com.vaadin.flow.component.Text;

import com.vaadin.flow.router.Route;

@Route
public class CourseView extends Abstraktview{

    public CourseView() {
        initView();

        add(new Text("Kurzus adatai:"));
        add(new Text("Azonosító(id), "));
        add(new Text("Név(name)"));

    }

}
